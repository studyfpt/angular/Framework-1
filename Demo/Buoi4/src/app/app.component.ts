import { Component } from '@angular/core';
import { IProduct } from '../interface/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  products: IProduct[] = [
    {
      name: "Product 1",
      image: "https://picsum.photos/200",
      price : 1000
    }
  ]
  addProduct = (data : IProduct) => {
    this.products.push(data)
    console.log(this.products);

  }
}
