import { Component } from '@angular/core';
import { INav } from '../../../interface/nav';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
    navs : INav[] = [
      {
        id:1,
        name:'Home',
        path:'home',
        parent:0
      },
      {
      id:2,
      name:'About us',
      path:'about-us',
      parent:0
      },
      {
      id:3,
      name:'Products',
      path:'product',
      parent:0
      },
      {
      id:4,
      name:'Fashion',
      path:'fashion',
      parent:3
      },
      {
      id:5,
      name:'Jewelry',
      path:'jewelry',
      parent:3
      },
      {
      id:6,
      name:'Contact',
      path:'contact',
      parent:0
      }
  ]
  item : number = -1
  showItem = (id: number) => {
    if(this.item == id) {
      this.item = -1
    } else {
      this.item = id
    }

  }
}
