import { Component, Input } from '@angular/core';
import { IProduct } from './../../../interface/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {
  @Input() product: IProduct = {} as IProduct
}
