import { Component, Input } from '@angular/core';
import { IProduct } from '../../../interface/product';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent {
  @Input() addProduct: any
  name = ""
  image = "https://picsum.photos/1"
  price = 0
  onAddProduct = () => {
    const data: IProduct = {
      name : this.name,
      image : this.image,
      price : this.price,
    }
    this.addProduct(data);
    alert('Thêm Sản Phẩm Thành Công !');
  };
}
